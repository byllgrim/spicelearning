# Spice Learning

How to do analog development with FLOSS?


## Resources

Simulators:
* eispice
* gnucap
* ngspice
* qucs
* qucs-s
* spice opus
* xyce
* ?

Schematic Capturers:
* gschem
* kicad
* oregon
* xcircuit

Spice Commands:
* listing
* op
* print V(a)
* print all
* source


## Goals

There are two goals:
1) Learn analog design.
2) Learn open source tools.

Level 1 Circuits:
* Common source/drain/gate
* Voltage divider
* Diode-connected transistor
* Current mirror
* Current source
* Comparator
* Operational amplifier

Level 2 Circuits:
* Switched-capacitor resistor
* Two-stage amplifier
* Analog-to-digital converter
* Digital-to-analog converter
* Preamplifier
* Audio power amplifier
* Pulse-width modulator

Level ? Circuits:
* Power amplifier
* Transformer
* DC-to-DC Converter
* Oscillator
* Power inverter
* Power supply
* Voltage regulator
* Detector
* Voltage reference
* Current limiter

RF Circuits:
* RC/RL/LC/RLC filter
* Tuner
* Frequency mixer
* Modulator

Misc Circuits:
* Snubber
* Vibrator
* Rectifier
* Differentiator/Integrator
* Level shifter
* Surge protector
* Capacitance multiplier
* Crowbar
* Voltage-controlled oscillator
* Instrumentation amplifier

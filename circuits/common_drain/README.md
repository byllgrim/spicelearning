# Common Drain Amp

Resources:
* https://en.wikipedia.org/wiki/Common_drain

AKA "source follower".
AKA "stabilizer".
Used as: Voltage buffer ("a more ideal voltage source").

Characteristics:
* I gain:
    * `A_i  =  i_out / i_in  =  infty`
    * (gate is input)
* V gain:
    * `A_v  =  v_out / v_in`
    * `=  (g_m * R_s) / (g_m * R_s + 1)`
    * `=  1`
* In  impedance:
    * `r_in  =  v_in / i_in`
    * `=  infty`
* Out impedance:
    * `r_out  =  v_out / i_out`
    * `=  R_s || 1/g_m`
    * `=  R_s / (g_m * R_s + 1)`
    * `=  1/g_m`

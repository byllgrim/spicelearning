# Comparator (CMP)

Compares inputs and tells which is higher.

Pins:
* Two analog inputs
* One digital output

Metrics:
* Response time (delay)
* Current consumption
* Supply current
* Hysteresis
* Reference voltage
* Input offset voltage
* Input range
* Resistance

Misc:
* Reference divider
* Reference source

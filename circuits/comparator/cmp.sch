v 20201216 2
N -2000 0 0 0 4
{
T -1900 50 5 10 1 1 0 0 1
netname=in
}
T -2000 1500 9 12 1 0 0 0 1
TODO don't use ideal amp, make own
C -1200 -1100 1 270 0 voltage-3.sym
{
T -700 -1400 5 10 1 0 270 0 1
refdes=Vref
T -1200 -1100 5 10 1 0 0 0 1
value=2.5
}
C -2200 -1100 1 270 0 voltage-3.sym
{
T -1700 -1400 5 10 1 0 270 0 1
refdes=Vin
T -2200 -1100 5 10 1 0 0 0 1
value=2.5
}
C -2100 -2800 1 0 0 gnd-1.sym
{
T -1800 -2750 5 10 0 0 0 0 1
net=GND:1
}
C -1100 -2800 1 0 0 gnd-1.sym
{
T -800 -2750 5 10 0 0 0 0 1
net=GND:1
}
N -2000 -2000 -2000 -2500 4
N -1000 -2000 -1000 -2500 4
N -2000 0 -2000 -1100 4
N -1000 -1100 -1000 -400 4
N -1000 -400 0 -400 4
{
T -900 -350 5 10 1 1 0 0 1
netname=ref
}
C 400 -2800 1 0 0 gnd-1.sym
{
T 700 -2750 5 10 0 0 0 0 1
net=GND:1
}
N 500 -2500 500 -600 4
N 1000 -200 2500 -200 4
{
T 1900 -150 5 10 1 1 0 0 1
netname=out
}
C 0 -600 1 0 0 opamp-1.sym
{
T 700 0 5 10 1 0 0 0 1
refdes=X1
T 0 -600 5 10 1 0 0 0 1
model-name=myopamp
}
C -3200 -1100 1 270 0 voltage-3.sym
{
T -2700 -1400 5 10 1 0 270 0 1
refdes=Vdd
T -3200 -1100 5 10 1 0 0 0 1
value=5
}
C -3100 -2800 1 0 0 gnd-1.sym
{
T -2800 -2750 5 10 0 0 0 0 1
net=GND:1
}
N -3000 -2000 -3000 -2500 4
N 500 200 500 1000 4
N 500 1000 -3000 1000 4
N -3000 1000 -3000 -1100 4
C 1500 -2100 1 0 0 spice-directive-1.sym
{
T 1600 -1700 5 10 1 1 0 0 1
refdes=A1
T 1600 -2600 5 10 1 1 0 0 3
value=.subckt myopamp in1 in2 vdd vss out gain=1MEG
E1 out 0 VALUE = {LIMIT(V(in1,in2)*gain,V(vss),V(vdd))}
.ends myopamp
}

# DAC

Making an 8-bit Digital-to-Analog Converter


Resources:
* https://en.wikipedia.org/wiki/Digital-to-analog_converter


Types:
* PWM
* Delta-sigma
* Binary-weighted (Switched R/I/C, R-2R, ...)
* Successive approximation
* etc...


Metrics:
* Performance:
  * Resolution
  * Sampling rate
  * Monotonicity
  * Total harmonic distortion and noise (THD+N)
  * Dynamic range
  * Phase distortion
  * Jitter
* Static:
  * Differential nonlinearity (DNL)
  * Integral nonlinearity (INL)
  * Gain error
  * Offset error
  * Noise
* Frequency:
  * Spurious-free dynamic range (SFDR)
  * Signal-to-noise and distortion (SINAD)
  * i-th harmonic distortion (HDi)
  * Total harmonic distortion (THD)
* Time:
  * Glitch impulse area

default:
	@echo read the makefile

schem:
	gschem  $(DESIGN).sch

net:
	gnetlist  -g spice-sdb  -o $(DESIGN).net  $(DESIGN).sch

sim:
	cat  sim.net  $(DESIGN).net  >  xyce.net
	Xyce  xyce.net

plot:
	octave pdf.m

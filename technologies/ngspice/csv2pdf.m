# Convert CSV file to PDF file


## Validate cli args

arglist = argv();

if (nargin != 2)
  printf("usage: %s <csv> <pdf>\n", program_name())
  exit()
endif


## Gather table info and data

csvname = arglist{1};
pdfname = arglist{2};
csvdata = csvread(csvname);
nrows   = size(csvdata)(1);
ncols   = size(csvdata)(2);

if (ncols < 2)
  printf("error: Need at least 2 columns\n")
  exit()
endif


## Gather header data

csvfile    = fileread(csvname);
fid        = fopen(csvname);
headerline = fgetl(fid);
colnames   = strsplit(headerline, ',');


## Plot the figure

figure('visible', 'off')

hold on

for i = 2:ncols
  xdata = csvdata(:, 1);
  ydata = csvdata(:, i);

  plot(xdata, ydata)
endfor

xlabel(colnames{1})
legend(colnames{2:ncols})
print(pdfname, '-dpdf')

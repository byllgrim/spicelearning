module cnt (
  input wire  clk,
  input wire  rst,

  output reg [7:0]  y
);

  always @(posedge clk) begin
    if (rst) begin
      y <= 8'd 0;
    end else begin
      y <= y + 1;
    end
  end

endmodule
